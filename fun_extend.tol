//////////////////////////////////////////////////////////////////////////////
Serie ExtendF(Serie serie, Real period, Real differences, Real times)
//////////////////////////////////////////////////////////////////////////////
{
  Polyn p = (1-B^period)^differences;
  Serie ds = p:serie;
  Serie dsExt = SubSer(BinGroup(">>", For(1, times, Serie (Real n) {
    (B^(period*n)):ds
  })), Succ(Last(ds), Dating(ds), 1), TheEnd);
  serie >> DifEq(1/p, dsExt, serie)
};

//////////////////////////////////////////////////////////////////////////////
Serie ExtendB(Serie serie, Real period, Real differences, Real times)
//////////////////////////////////////////////////////////////////////////////
{ Reverse2000_Serie(ExtendF(Reverse2000_Serie(serie), period, differences, times)) };

//////////////////////////////////////////////////////////////////////////////
Serie ExtendFB(Serie serie, Real period, Real differences, Real times)
//////////////////////////////////////////////////////////////////////////////
{ ExtendF(ExtendB(serie, period, differences, times), period, differences, times) };

//////////////////////////////////////////////////////////////////////////////
Serie ExtendFB_FixedIMA(Serie serie, Real period, Real differences, Real times, 
  Real phi)
//////////////////////////////////////////////////////////////////////////////
{
  If(CountS(serie) > (1+differences)*period+1, { // >p+1
    Polyn polyn = (1-B^period)^differences;
    Polyn sDif = If(period==1, 1, 1-B^period); 
    Polyn sMA = If(period==1, 1, 1-phi*B^period); 
    ForecastingEasy(serie, 1, 0, 
      Polyn (1-B)*sDif*polyn, Polyn 1, Polyn (1-phi*B)*sMA, 
      period*times, period*times)
  }, {
    WriteLn("Nmero de datos insuficiente", "E");
    If(False, ?)
  })
};

//////////////////////////////////////////////////////////////////////////////
Serie ExtendFB_IMA(Serie serie, Real period, Real differences, Real times)
//////////////////////////////////////////////////////////////////////////////
{
  If(CountS(serie) > (2+differences)*period+2, { // >2*(p+1)
    Polyn polyn = (1-B^period)^differences;
    Polyn sDif = If(period==1, 1, 1-B^period); 
    Polyn sMA = If(period==1, 1, 1-0.8*B^period); 
    Real Show(0, "ALL");
    Set est = Estimate(@ModelDef(Copy(serie), 1, 0, 
      period, 1, Polyn (1-B)*sDif*polyn,
      SetOfPolyn(1, 1), SetOfPolyn(1-0.8*B, sMA), 
      Copy(Empty), Copy(Empty)));
    Real Show(1, "ALL");
    ForecastingEasy(serie, 1, 0, 
      Polyn (1-B)*sDif*polyn, Polyn 1, Polyn SetProd(est[2][8]), 
      period*times, period*times)
  }, {
    WriteLn("Nmero de datos insuficiente", "E");
    If(False, ?)
  })
};

//////////////////////////////////////////////////////////////////////////////
Serie ExtendFB_MassiveArima(Serie serie, Real period, Real times)
//////////////////////////////////////////////////////////////////////////////
{
  Real nError = Copy(NError);
  Real Eval("Real {#Require MassiveArima; 1}");
  If(nError==NError, {
    Real defTimes = If(IsFinite(times), times, 2);
    Real Show(0, "ALL");
    NameBlock Eval("MassiveArima::@AutoArima autoArima = [[ 
      Real period = "<<period<<";
      Real numPerFcstB = "<<defTimes<<";
      Real numPerFcstF = "<<defTimes<<" 
    ]]");
    NameBlock fcst = autoArima::Run(serie);
    Real Show(1, "ALL");
    fcst::extended
  })
};

//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
Serie ExtendF_Border(Serie serie, Real period, Real times)
//////////////////////////////////////////////////////////////////////////////
{
  Serie SubSer(
    Concat(CalInd(C, Dating(serie))*FirstS(serie), serie,First(serie));
	Succ(First(serie), Dating(serie), -period*times),
	TheEnd
  )
};

//////////////////////////////////////////////////////////////////////////////
Serie ExtendB_Border(Serie serie, Real period, Real times)
//////////////////////////////////////////////////////////////////////////////
{ Reverse2000_Serie(ExtendF_Border(Reverse2000_Serie(serie), period, times)) };

//////////////////////////////////////////////////////////////////////////////
Serie ExtendFB_Border(Serie serie, Real period, Real times)
//////////////////////////////////////////////////////////////////////////////
{ ExtendF_Border(ExtendB_Border(serie, period, times), period, times) };
//////////////////////////////////////////////////////////////////////////////
