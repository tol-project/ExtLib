//////////////////////////////////////////////////////////////////////////////
// FILE    : fun_truncated_normal.tol
// PURPOSE : Funciones para calcular los estad�sticos y una muestra de 
//           longitud dada de una Distribuci�n Normal  X~N(mu,sigma*sigma) 
//           donde a < X < b y -inf <= a < b <= inf, i.e., X es una 
//           Distribuci�n Normal Truncada.
//           Notar que los inputs mu y sigma son los momentos de la 
//           Distribuci�n Normal sin truncar. 
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
Real TruncatedNormal.Mean(Real mu, Real sigma, Real a, Real b) 
//////////////////////////////////////////////////////////////////////////////
{
  Real If(IsUnknown(mu+sigma+a+b),{
    WriteLn(_Distribution.InputErrors(199),"E");?}
   ,{Real If(sigma<=0,{WriteLn(_Distribution.InputErrors(101),"E");?},
    {If(b<=a,{WriteLn(_Distribution.InputErrors(102),"E");?},{
    Real alpha = (a-mu)/sigma;
    Real beta = (b-mu)/sigma;
    Real zeta = DistNormal(beta)-DistNormal(alpha);
    Real fa = DensNormal(alpha);
    Real fb = DensNormal(beta);
    Real mu + (fa-fb)/zeta * sigma}
	)})})
};

//////////////////////////////////////////////////////////////////////////////
PutDescription("Calcula la media de una Distribuci�n Normal Truncada, donde "
  "los argumentos mu y sigma son los momentos de la Distribuci�n Normal sin "
  "truncar y a y b los extremos.", TruncatedNormal.Mean);

//////////////////////////////////////////////////////////////////////////////
Real TruncatedNormal.Variance(Real mu, Real sigma, Real a, Real b) 
//////////////////////////////////////////////////////////////////////////////
{
  Real If(IsUnknown(mu+sigma+a+b),
   {WriteLn(_Distribution.InputErrors(199),"E");?}
   ,{Real If(sigma<=0,{WriteLn(_Distribution.InputErrors(101),"E");?},
    {If(b<=a,{WriteLn(_Distribution.InputErrors(102),"E");?},{
    //En el caso de que no haya errores en los argumentos:
    Real variance = sigma*sigma;
    Real alpha = (a-mu)/sigma;
    Real beta = (b-mu)/sigma;
    Real zeta = DistNormal(beta)-DistNormal(alpha);
    Real fa = DensNormal(alpha);
    Real fb = DensNormal(beta);
    Real afa = If(IsFinite(alpha), alpha*DensNormal(alpha), 0);
    Real bfb = If(IsFinite(beta), beta*DensNormal(beta), 0);
    Real variance * (1 + (afa-bfb)/zeta - ((fa-fb)/zeta)^2) }
    )})})
};
//////////////////////////////////////////////////////////////////////////////
PutDescription("Calcula la varianza de una Distribuci�n Normal Truncada, "
  "donde los argumentos mu y sigma son los momentos de la Distribuci�n "
  "Normal sin truncar y a y b los extremos.", TruncatedNormal.Variance);

//////////////////////////////////////////////////////////////////////////////
Matrix TruncatedNormal.Sample(Real mu, Real sigma, Real a, Real b, Real len) 
//////////////////////////////////////////////////////////////////////////////
{
  Matrix If(IsUnknown(mu+sigma+a+b+len) , { 
    WriteLn(Text _Distribution.InputErrors(Real 199),"E"); UnknownMatrix }, {
    Matrix If(b<=a,{ WriteLn(Text _Distribution.InputErrors(Real 102),"E"); 
    UnknownMatrix},
    {Matrix If(!(len>0),{
     WriteLn(Text _Distribution.InputErrors(Real 103),"E"); UnknownMatrix},
    {Matrix If(sigma<=0,{
     WriteLn(Text _Distribution.InputErrors(Real 199),"E"); UnknownMatrix},
    //Ahora vienen las sentencias cuando no hay errores en los input:
    {
    Real If(!(IsInteger(len)),{
    WriteLn(Text _Distribution.InputWarnings(Real 501),"W");
    Real len:=Ceil(len) });
    Real variance = sigma*sigma;
    Real alpha = (a-mu)/sigma;
    Real beta = (b-mu)/sigma;
    Real zeta = DistNormal(beta)-DistNormal(alpha);
    Real teor_i = len/zeta;
    Matrix m = Gaussian(1, teor_i * 1.1, mu, sigma);
    Matrix wF = SetRow(Select(MatSet(m)[1], Real (Real x) {
      x>=a & x<=b
    }));
    //!! Podr�a ocurrir que el tama�o de wF fuese menor del esperado:
    Text If(Columns(wF)<len,
      {WriteLn("Longitud muestra:"<<Columns(wF));
       WriteLn(Text _Distribution.InputWarnings(Real 502),"W")});
    Matrix Tra(Sub(wF, 1, 1, 1, len))})}
	)})})
};
//////////////////////////////////////////////////////////////////////////////
PutDescription("Calcula una muestra de longitud len que sigue una "
  "Distribuci�n Normal Truncada, donde los argumentos mu y sigma son "
  "los momentos de la Distribuci�n Normal sin truncar "
  "y a y b los extremos.", TruncatedNormal.Sample);
  
//////////////////////////////////////////////////////////////////////////////
Text _Distribution.InputErrors(Real errorType)
//////////////////////////////////////////////////////////////////////////////
{
Text Case( 
  errorType==101,
  Text err1="La desviaci�n sigma debe ser estrictamente positiva.",

  errorType==102, 
  Text err2="La cota b debe ser estrictamente superior a la cota "
            "inferior a.",

  errorType==103,
  Text err3="La longitud len de la muestra debe ser un entero positivo.",

  errorType==199,
  Text err3="Se ha introducido un valor omitido como argumento."
         )
};

//////////////////////////////////////////////////////////////////////////////
Text _Distribution.InputWarnings(Real warningType)
//////////////////////////////////////////////////////////////////////////////
{
Text Case( 
  warningType==501,
  Text war1="La longitud len debe ser un entero positivo. \nSe redondea "
            "len mediante la funci�n Ceil.",

  warningType==502,
  Text war1="La longitud de la muestra resultante es menor a len.",


  warningType==599,
  Text war2="Se ha introducido un valor omitido como argumento."
         )
};

