//////////////////////////////////////////////////////////////////////////////
// FILE    : fun_log_normal.tol
// PURPOSE : Funciones para calcular los estadísticos de una Distribución 
//           Log-normal. Si X~Log-normal, entonces los argumentos mu y sigma 
//           son los momentos de la Distribución Normal, Y~Normal(mu,sigma), 
//           con log(X)=Y. 
//////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////
Matrix LogNormal.Sample(Real mu, Real sigma, Real a, Real b, Real len) 
//////////////////////////////////////////////////////////////////////////////
{
  Matrix If(IsUnknown(mu+sigma+a+b+len) , { WriteLn(Text _Distribution.InputErrors(Real 199),"E"); UnknownMatrix
   }, { Matrix If(b<=a,{ WriteLn(Text _Distribution.InputErrors(Real 102),"E"); UnknownMatrix},
    {Matrix If(!(len>0),{ WriteLn(Text _Distribution.InputErrors(Real 103),"E"); UnknownMatrix},
	{ Matrix If(sigma<=0,{WriteLn(Text _Distribution.InputErrors(Real 199),"E");UnknownMatrix},
    //Ahora vienen las sentencias cuando no hay errores en los input:
    {
    Real If(!(IsInteger(len)),{WriteLn(Text _Distribution.InputWarnings(Real 501),"W");Real len:=Ceil(len) });
    Matrix m  = Gaussian(1, len, 0,1);
    Matrix wF = EvalMat(m,Real (Real x){Exp(mu+sigma*x)});
    Matrix Tra(Sub(wF, 1, 1, 1, len))})}
	)})})
};
//////////////////////////////////////////////////////////////////////////////
PutDescription("Calcula una muestra de longitud len que sigue una "
  "Distribución Log-Normal, donde los argumentos mu y sigma son "
  "los momentos de la Distribución Normal "
  "y a y b los extremos.", LogNormal.Sample);

//////////////////////////////////////////////////////////////////////////////
Real LogNormal.Mean(Real mu, Real sigma) 
//////////////////////////////////////////////////////////////////////////////
{
  If(IsUnknown(mu+sigma),{WriteLn(_Distribution.InputErrors(199),"E");?},{
    If(sigma<=0, {
      WriteLn(_Distribution.InputErrors(101),"E");
      ?}, {
    //En el caso de que no haya errores en los input:
    Real Exp(mu + .5*sigma*sigma)
  })})
};
//////////////////////////////////////////////////////////////////////////////
PutDescription("Calcula la media de una Distribución Log-normal , donde "
  "los argumentos mu y sigma son los momentos de la Distribución Normal "
  "de la que se parte, i.e., si X~Log-normal, entonces Y~Normal(mu,sigma) "
  "con log(X)=Y.", LogNormal.Mean);

//////////////////////////////////////////////////////////////////////////////
Real LogNormal.Variance(Real mu, Real sigma) 
//////////////////////////////////////////////////////////////////////////////
{
  If(IsUnknown(mu+sigma),{WriteLn(_Distribution.InputErrors(199),"E");?},{
    If(sigma<=0, {
      WriteLn(_Distribution.InputErrors(101),"E");
      ?}, {
    //En el caso de que no haya errores en los input:
    Real (Exp(sigma*sigma)-1)*Exp(2*mu+sigma*sigma)
  })})
};
//////////////////////////////////////////////////////////////////////////////
PutDescription("Calcula la varianza de una Distribución Log-normal , donde "
  "los argumentos mu y sigma son los momentos de la Distribución Normal "
  "de la que se parte, i.e., si X~Log-normal, entonces Y~Normal(mu,sigma) "
  "con log(X)=Y.", LogNormal.Variance);
  
//////////////////////////////////////////////////////////////////////////////
Real LogNormal.Mode(Real mu, Real sigma) 
//////////////////////////////////////////////////////////////////////////////
{
  If(IsUnknown(mu+sigma),{WriteLn(_Distribution.InputErrors(199),"E");?},{
    If(sigma<=0, {
      WriteLn(_Distribution.InputErrors(101),"E");
      ?}, {
    //En el caso de que no haya errores en los input:
    Real Exp(mu - sigma*sigma)
  })})
};
//////////////////////////////////////////////////////////////////////////////
PutDescription("Calcula la moda de una Distribución Log-normal , donde "
  "los argumentos mu y sigma son los momentos de la Distribución Normal "
  "de la que se parte, i.e., si X~Log-normal, entonces Y~Normal(mu,sigma) "
  "con log(X)=Y.", LogNormal.Mode);
  
//////////////////////////////////////////////////////////////////////////////
Real LogNormal.Median(Real mu) 
//////////////////////////////////////////////////////////////////////////////
{ 
If(IsUnknown(mu),{WriteLn(_Distribution.InputErrors(199),"E");?},
  Real Exp(mu) )};
//////////////////////////////////////////////////////////////////////////////
PutDescription("La mediana de una dist. Log-Normal es Exp(mu), con mu la "
  "media de la dist. Normal de la que procede. Si X~Log-normal, entonces"
  " Y~Normal(mu,sigma) con log(X)=Y.", LogNormal.Median);  

/* 
// NOTA:Las siguientes funciones estan en el archivo fun_truncated_normal.tol
//////////////////////////////////////////////////////////////////////////////
Text _Distribution.InputErrors(Real errorType)
//////////////////////////////////////////////////////////////////////////////
{
Text Case( 
  errorType==101,
  Text err1="La desviación sigma debe ser estrictamente positiva.",

  errorType==102, 
  Text err2="La cota b debe ser estrictamente superior a la cota "
            "inferior a.",

  errorType==103,
  Text err3="La longitud len de la muestra debe ser un entero positivo.",

  errorType==199,
  Text err3="Se ha introducido un valor omitido como argumento."
         )
};

//////////////////////////////////////////////////////////////////////////////
Text _Distribution.InputWarnings(Real warningType)
//////////////////////////////////////////////////////////////////////////////
{
Text Case( 
  warningType==501,
  Text war1="La longitud len debe ser un entero positivo. \nSe redondea "
            "len mediante la función Ceil.",

  warningType==599,
  Text war2="Se ha introducido un valor omitido como argumento."
         )
};
*/
